# Introduction

This is a technical test for **Frontend Developers** and is part of the hiring process at
 [OnGres](https://ongres.com). Plase, read this README carefully since it is
 a requirement in order to perform this test correctly.

Other requirements to perform this test are:

* A workstation that you will use to perform the test
* Your favorite frontend development environment/framework/tools ready to be used
* A [GitLab account](https://gitlab.com/users/sign_up) (If you do not own one. But if you
 are reading this you probably have one already!)

## What do we expect?

We expect that the amount of effort to do any of these exercises won't take too many hours of your day.
 We understand that your time is valuable, and in anyone's busy schedule that constitutes a fairly
 substantial chunk of time, so we really appreciate any effort you put into helping us build a
 solid team. 
 
 For this reason this test is totally asynchronous and can be completed at any time
 within a time frame of 10 days starting from when this repository is cloned. Also, try to spend a maximum of 4 hours to complete the test. 
 
 Do not worry if you are not able to solve some goals, the objective is not only to prove you can perform a real task but to make sure your technical skills and your decision making process align with what we are looking for.
 
 If needed, there might be a follow-up interview where you will have the opportunity to discuss, defend or even apply any possible fix on your submission.

## What are we looking for?

Keep it simple but effective. Really. We really don't want you spending too much more time on it.

> Treat it like production code. But Keep it simple.

That is, develop your software in the same way that you would for any code that is intended to be
 deployed to production. These may be toy exercises, but we really would like to get an idea of how
 you build code on a day-to-day basis.

## How to submit?

The typical workflow we follow here at OnGres is to work on an issue ticket and create a branch to
 work on it. When the work is completed we create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
 against the `main` branch in order to be reviewed by another colleague. 
 
 The workflow we require you
 to follow has some modification due to some restrictions we need to apply in order to ensure your
 work is not shared with other candidates. If you do not want to do so (see also the [#FAQ](#FAQ)
 section).

The workflow you have to follow is here divided into steps:

1. [Fork this project](https://gitlab.com/ongresinc/interview/frontend-developer/-/forks/new) to a
 private project you own
2. Perform the test exercise on your workstation by [cloning your private project](https://docs.gitlab.com/ee/user/project/repository/#clone-a-repository)
 and push all the code to your private project in one or more branches
3. Create one or more [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
 in your private project against the `main` branch with the solution to the exercises
4. Fill the merge request description with any detail you may want to include and/or requested
 by the specific exercise.
5. [Invite to your private project](https://docs.gitlab.com/ee/user/project/members/) with Reporter
 role the following users: @jose_oss_pg @ldgarcia
6. Send an email to `werehiring@ongres.com` with your name and the URL to your private repository.
 After reviewing your work we will schedule the follow-up interview.

## The Exercises

The main idea is to work on a project to track hour entries from our Professional Services team. These hour entries will let us know exactly how much time we spend on each task for each one of our customers.

This project is basically a [CRUD](https://developer.mozilla.org/en-US/docs/Glossary/CRUD) application, in the sense that it reads information from a REST API which can be shown in the form of data tables whose records can be either Created, Updated or Deleted.

Now, let's assume our backend team has implemented a real REST API server, but... In reality, the response from each endpoint is a JSON file stored in the `/api/` folder located in the root of the repo mentioned above. Each response is named according to the following endpoints:

* `/customers`: provides basic information about our customers. Like the customer's name, contact info and their status.
* `/projects`: provides information about the different projects we run for each of our customers. Like the project's name, the amount of support hours available or its start and end date.
* `/hourentries`: provides information about the time dedicated by one of our engineers to execute a single task for a specific project.

For further details on the information contained in each endpoint's response, we provide a JSON spec located in the `/info` folder. There, for each resource property, you'll find:

* The name of the property
* The human-readable title
* A flag to indicate if it's mandatory or not
* The data type (`number`, `text`, `email`, `date`...)
* The data restrictions (ie. `min` or `max` value when the data type is `number`)

And now, get ready to code! 

### Exercise #1 - It's CRUD time!

#### Description

You are required to develop a simple application with a UI that displays the records included in the response from the three different endpoints mentioned in the previous section.

These records can be shown on tables or any other data structures you might find useful for this purpose.

Since our customers and projects are managed independently by our management team, we'll focus on operations made by our engineers over the `hourentries` resource.

#### Goals

* [ ] Show each hour entry along with its data, including its project and customer name
* [ ] Include functional EDIT buttons for each hour entry
* [ ] Show a notification for every EDIT submission
* [ ] Document how to run the application


### Exercise #2 - It's testing time!

#### Description

We know a well written codebase should always have good test coverage, but for this project, we were lazy and haven't thought much about what's the most relevant test we should start with...

- Should we test first the buttons to EDIT or DELETE records?
- Or should we start with the submission made by an EDIT action?
- Or perhaps we should simply test the form renders correctly?

We are not 100% sure about any (or none) of the aforementioned assumptions, so it's up to you to decide where to start.

#### Goals

* [ ] Implement the test you think it's the most relevant
* [ ] Document how to run the tests

## Considerations

At OnGres we work with [VueJS](https://vuejs.org/) as our JS framework of choice, but we know there are several popular alternatives out there. So for this reason, you are free to choose your desired framework for this task.

To try different responses when submitting changes to the REST API, you can use [https://httpstat.us/](https://httpstat.us/)

Our testing tool of choice is [Cypress](https://www.cypress.io/), but any other testing framework you feel comfortable with is welcome.

Also, you can use any other tools and dependencies that you consider to be helpful to achieve these goals, but we expect to hear a good reason to back up your decision. 


## FAQ

### Do you have to complete all the exercises?
Ideally yes, but don’t worry if you don’t have enough time
 to finish all the exercises. We’re going to evaluate if the code is clean enough and the correctness of the
 solution.

### Is it OK to share your solutions publicly?
Yes, the questions are not prescriptive, the process
 and discussion around the code is the valuable part. You do the work, you own the code. We specify
 in the  Given we
 are asking you to give up your time, it is entirely reasonable for you to keep and use your
 solution as you see fit.

### Should I do X?

For any value of X, it is up to you! We will leave it up to you to provide us with what you see as important. Just remember the rough time frame of the project. If it is going to take you a couple of days, it isn't essential.

### Something is ambiguous, and I don't know what to do?
The first thing is: don't get stuck. We
 really don't want to trip you up intentionally, we are just attempting to see how you approach
 problems. 
 
 If you really feel stuck, our first preference is for you to make a decision and
 document it within your submission. If you feel it is not possible to do this, just send us an email
 and we will try to clarify or correct the question for you.

### **Good luck!**
